package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.referenceTab

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.smellydogcoding.westvirginiaelectronicfieldguide.databinding.ReferenceListViewItemBinding
import com.smellydogcoding.westvirginiaelectronicfieldguide.databinding.RulesListViewItemBinding
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.ReferenceDocs

class TabReferenceRecyclerViewAdapter(private val clickListener: ReferencesListListener) : ListAdapter<ReferenceDocs, TabReferenceRecyclerViewAdapter.ReferencesListViewHolder>(TabReferencesAdapterDiffCallback()) {

    class ReferencesListViewHolder(private val binding: ReferenceListViewItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(clickListener: ReferencesListListener, item: ReferenceDocs) {
            binding.reference = item
            binding.referenceText.text = item.name
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }
    }

    class TabReferencesAdapterDiffCallback : DiffUtil.ItemCallback<ReferenceDocs>() {
        override fun areItemsTheSame(oldItem: ReferenceDocs, newItem: ReferenceDocs): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: ReferenceDocs, newItem: ReferenceDocs): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReferencesListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = ReferenceListViewItemBinding.inflate(inflater)
        itemView.referenceHolder.minWidth = parent.width
        return ReferencesListViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ReferencesListViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(clickListener, item)
    }

    class ReferencesListListener(val clickListener: (name: String, filePath: String) -> Unit) {
        fun onClick(rule: ReferenceDocs) = clickListener(rule.name, rule.path)
    }

}