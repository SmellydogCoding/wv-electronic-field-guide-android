package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.rulesTab

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.smellydogcoding.westvirginiaelectronicfieldguide.databinding.RulesListViewItemBinding
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.RuleDocs

class TabRulesRecyclerViewAdapter(private val clickListener: RulesListListener) : ListAdapter<RuleDocs, TabRulesRecyclerViewAdapter.RulesListViewHolder>(TabRulesAdapterDiffCallback()) {

    override fun onBindViewHolder(holder: RulesListViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(clickListener, item)
    }

    class RulesListViewHolder(private val binding: RulesListViewItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(clickListener: RulesListListener, item: RuleDocs) {
            binding.rule = item
            binding.ruleText.text = item.name
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RulesListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = RulesListViewItemBinding.inflate(inflater)
        itemView.ruleHolder.minWidth = parent.width
        return RulesListViewHolder(itemView)
    }

    class TabRulesAdapterDiffCallback : DiffUtil.ItemCallback<RuleDocs>() {
        override fun areItemsTheSame(oldItem: RuleDocs, newItem: RuleDocs): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: RuleDocs, newItem: RuleDocs): Boolean {
            return oldItem == newItem
        }

    }

    class RulesListListener(val clickListener: (name: String, filePath: String) -> Unit) {
        fun onClick(rule: RuleDocs) = clickListener(rule.name, rule.path)
    }

}