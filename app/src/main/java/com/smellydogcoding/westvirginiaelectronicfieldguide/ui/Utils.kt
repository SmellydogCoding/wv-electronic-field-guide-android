package com.smellydogcoding.westvirginiaelectronicfieldguide.ui

import android.app.Application
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.smellydogcoding.westvirginiaelectronicfieldguide.R
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.home.ProgramList
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

@BindingAdapter("programIcon")
fun ImageView.setProgramIcon(item: ProgramList?) {
    item?.let {
        setImageResource(when (item.icon) {
            "ic_utensils_solid" -> R.drawable.ic_utensils_solid
            "ic_hotel_solid" -> R.drawable.ic_hotel_solid
            "ic_tint_solid" -> R.drawable.ic_tint_solid
            "ic_toilet_solid" -> R.drawable.ic_toilet_solid
            "ic_swimmer_solid" -> R.drawable.ic_swimmer_solid
            "ic_anchor_solid" -> R.drawable.ic_anchor_solid
            "ic_caravan_solid" -> R.drawable.ic_caravan_solid
            "ic_baby_solid" -> R.drawable.ic_baby_solid
            "ic_paw_solid" -> R.drawable.ic_paw_solid
            "ic_cloud_showers_heavy_solid" -> R.drawable.ic_cloud_showers_heavy_solid
            "ic_search_solid" -> R.drawable.ic_search_solid
            else -> R.drawable.ic_search_solid
        })
    }
}

object JsonAsset {
    lateinit var application: Application
    inline fun <reified T: Any> getJson (name: String): List<T>? {

        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
        val json = application.assets.open(name).bufferedReader().use{ it.readText() }
        val listType = Types.newParameterizedType(List::class.java, T::class.java)
        val adapter: JsonAdapter<List<T>> = moshi.adapter(listType)

        return adapter.fromJson(json)
    }
}
