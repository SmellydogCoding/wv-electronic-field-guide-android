package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.codeSearch

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.smellydogcoding.westvirginiaelectronicfieldguide.databinding.CodeSearchListItemGeneralBinding
import com.smellydogcoding.westvirginiaelectronicfieldguide.databinding.CodeSearchListItemRecreationalBinding
import com.smellydogcoding.westvirginiaelectronicfieldguide.databinding.CodeSearchListItemRetailBinding

class CodeSearchRecyclerViewAdapter(private val resultsData: List<*>, private val clickListener: CodeSearchListListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_RETAIL = 0
        private const val TYPE_GENERAL = 1
        private const val TYPE_RECREATIONAL = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = when (viewType) {
            TYPE_RETAIL -> {
                val itemView = CodeSearchListItemRetailBinding.inflate(LayoutInflater.from(parent.context))
                itemView.retailFoodResultHolder.minWidth = parent.width
                RetailFoodViewHolder(itemView)
            }
            TYPE_GENERAL -> {
                val itemView = CodeSearchListItemGeneralBinding.inflate(LayoutInflater.from(parent.context))
                itemView.generalSanitationResultHolder.minWidth = parent.width
                GeneralSanitationViewHolder(itemView)
            }
            TYPE_RECREATIONAL -> {
                val itemView = CodeSearchListItemRecreationalBinding.inflate(LayoutInflater.from(parent.context))
                itemView.recreationalWaterResultHolder.minWidth = parent.width
                RecreationalWaterViewHolder(itemView)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = resultsData[position]
        when (holder) {
            is RetailFoodViewHolder -> holder.bind(clickListener, item as RetailFoodRuleSet)
            is GeneralSanitationViewHolder -> holder.bind(clickListener, item as GeneralSanitationRuleSet)
            is RecreationalWaterViewHolder -> holder.bind(clickListener, item as RecreationalWaterRuleSet)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (resultsData[position]) {
            is RetailFoodRuleSet -> TYPE_RETAIL
            is GeneralSanitationRuleSet -> TYPE_GENERAL
            is RecreationalWaterRuleSet -> TYPE_RECREATIONAL
            else -> throw IllegalArgumentException()
        }
    }

    class CodeSearchListListener(val clickListener: ((rule: Any) -> Unit)) {
        fun onClick(rule: Any) = clickListener(rule)
    }

    override fun getItemCount(): Int {
        return resultsData.size
    }

    class RetailFoodViewHolder(private val binding: CodeSearchListItemRetailBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(clickListener: CodeSearchListListener, item: RetailFoodRuleSet) {
            binding.rule = item
            binding.ruleCitation.text = item.Code
            binding.ruleDescription.text = item.Description
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }
    }

    class GeneralSanitationViewHolder(private val binding: CodeSearchListItemGeneralBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(clickListener: CodeSearchListListener, item: GeneralSanitationRuleSet) {
            binding.rule = item
            binding.ruleCitation.text = item.Code
            binding.ruleDescription.text = item.Description
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }
    }

    class RecreationalWaterViewHolder(private val binding: CodeSearchListItemRecreationalBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(clickListener: CodeSearchListListener, item: RecreationalWaterRuleSet) {
            binding.rule = item
            binding.ruleCitation.text = item.Code
            binding.ruleDescription.text = item.Description
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }
    }

}