package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.smellydogcoding.westvirginiaelectronicfieldguide.R

class DocumentsFragment : Fragment() {

    private lateinit var sharedViewModel: DocumentsSharedViewModel
    private lateinit var viewModel: DocumentsViewModel
    private lateinit var viewPager2: ViewPager2
    private lateinit var documentsCollectionAdapter: DocumentsCollectionAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val program = DocumentsFragmentArgs.fromBundle(requireArguments()).program

        sharedViewModel = ViewModelProvider(requireActivity()).get(DocumentsSharedViewModel::class.java)
        viewModel = ViewModelProvider(this).get(DocumentsViewModel::class.java)

        val root = inflater.inflate(R.layout.fragment_documents, container, false)

        val tabLayout: TabLayout = root.findViewById(R.id.documents_tab_layout)

        documentsCollectionAdapter = DocumentsCollectionAdapter(this)
        viewPager2 = root.findViewById(R.id.documents_viewPager)
        viewPager2.adapter = documentsCollectionAdapter

        TabLayoutMediator(tabLayout, viewPager2) { tab, position ->
            when (position) {
                0 -> tab.text = "Rules"
                1 -> tab.text = "Procedures"
                2 -> tab.text = "Reference"
            }
        }.attach()

        sharedViewModel.getDocs(program)

        sharedViewModel.navigateToPDFViewerFilePath.observe(viewLifecycleOwner, {
            it?.let {
                val name = sharedViewModel.navigateToPDFViewerName.value.toString()
                this.findNavController().navigate(DocumentsFragmentDirections.actionDocumentsFragmentToPDFViewerFragment(it, name))
                sharedViewModel.onPDFViewerNavigated()
            }
        })

        return root
    }

}