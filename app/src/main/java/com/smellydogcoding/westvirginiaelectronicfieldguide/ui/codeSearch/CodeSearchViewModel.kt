package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.codeSearch

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.JsonAsset
import java.util.*


@Suppress("UNCHECKED_CAST")
class CodeSearchViewModel : ViewModel() {

    private val _selectedRuleset = MutableLiveData<List<*>>()
    private val selectedRuleset: LiveData<List<*>> get() = _selectedRuleset

    private val _results = MutableLiveData<List<*>>()
    val results: LiveData<List<*>> get() = _results

    private val _searchTerm = MutableLiveData<String>()
    val searchTerm : LiveData<String> get() = _searchTerm

    private val _ruleset = MutableLiveData<String>()
    val ruleset : LiveData<String> get() = _ruleset

    private val _ruleIsClicked = MutableLiveData<Boolean>()
    val ruleIsClicked : LiveData<Boolean> get() = _ruleIsClicked

    // Common
    private val _fullText = MutableLiveData<String>()
    val fullText : LiveData<String> get() = _fullText

    private val _code = MutableLiveData<String>()
    val code : LiveData<String> get() = _code

    private val _section = MutableLiveData<String>()
    val section : LiveData<String> get() = _section

    private val _description = MutableLiveData<String>()
    val description : LiveData<String> get() = _description

    // Retail Food
    private val _classification = MutableLiveData<String>()
    val classification : LiveData<String> get() = _classification

    private val _category = MutableLiveData<String>()
    val category : LiveData<String> get() = _category

    private val _compliance = MutableLiveData<String>()
    val compliance : LiveData<String> get() = _compliance

    // General Sanitation
    private val _followUpRecommended = MutableLiveData<String>()
    val followUpRecommended : LiveData<String> get() = _followUpRecommended

    // Recreational Water
    private val _closure = MutableLiveData<String>()
    val closure : LiveData<String> get() = _closure

    private val _notes = MutableLiveData<String>()
    val notes : LiveData<String> get() = _notes

    fun setRuleset(type: String) {
        _selectedRuleset.value = when (type) {
            "retailfood" -> JsonAsset.getJson<RetailFoodRuleSet>("food/retailFoodRules.json")
            "general" -> JsonAsset.getJson<GeneralSanitationRuleSet>("general/generalRules.json")
            "recwater" -> JsonAsset.getJson<RecreationalWaterRuleSet>("recwater/recwaterRules.json")
            else -> throw IllegalArgumentException()
        }
        _ruleset.value = type
        _results.value = null
    }

    fun search(term: String) {
        _searchTerm.value = term
        val pattern = term.toLowerCase(Locale.ROOT).toRegex()
        _results.value = when (ruleset.value) {
            "retailfood" -> {
                val rule = selectedRuleset.value as List<RetailFoodRuleSet>
                rule.filter { pattern.containsMatchIn(it.FullText.toLowerCase(Locale.ROOT)) || pattern.containsMatchIn(it.Description.toLowerCase(Locale.ROOT)) }
            }
            "general" -> {
                val rule = selectedRuleset.value as List<GeneralSanitationRuleSet>
                rule.filter { pattern.containsMatchIn(it.FullText.toLowerCase(Locale.ROOT)) || pattern.containsMatchIn(it.Description.toLowerCase(Locale.ROOT)) }
            }
            "recwater" -> {
                val rule = selectedRuleset.value as List<RecreationalWaterRuleSet>
                rule.filter { pattern.containsMatchIn(it.FullText.toLowerCase(Locale.ROOT)) || pattern.containsMatchIn(it.Description.toLowerCase(Locale.ROOT)) }
            }
            else -> throw IllegalArgumentException()
        }
    }

    fun onDocumentClicked(rule: Any) {
        when (rule) {
            is RetailFoodRuleSet -> {
                _fullText.value = rule.FullText
                _code.value = rule.Code
                _classification.value = rule.Classification
                _description.value = rule.Description
                _category.value = rule.Category
                _section.value = rule.Section
                _compliance.value = rule.Compliance
                _ruleIsClicked.value = true
            }
            is GeneralSanitationRuleSet -> {
                _fullText.value = rule.FullText
                _code.value = rule.Code
                _section.value = rule.Section
                _description.value = rule.Description
                _followUpRecommended.value = rule.FollowUpRecommended
                _ruleIsClicked.value = true
            }
            is RecreationalWaterRuleSet -> {
                _fullText.value = rule.FullText
                _code.value = rule.Code
                _section.value = rule.Section
                _description.value = rule.Description
                _closure.value = rule.Closure
                _notes.value = rule.Notes
                _ruleIsClicked.value = true
            }
        }
    }

    fun onResultsDetailsNavigated() {
        _fullText.value = null
        _code.value = null
        _classification.value = null
        _description.value = null
        _category.value = null
        _section.value = null
        _compliance.value = null
        _followUpRecommended.value = null
        _closure.value = null
        _notes.value = null
        _ruleIsClicked.value = false
    }

    fun restoreSearch(searchTerm: String, ruleSet: String) {
        setRuleset(ruleSet)
        search(searchTerm)
    }
}
