package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.codeSearch

import android.app.Activity
import android.util.Log
import android.view.View
import android.widget.AdapterView

class SpinnerActivity(val viewModel: CodeSearchViewModel) : Activity(), AdapterView.OnItemSelectedListener {
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent?.getItemAtPosition(position)) {
            "2013 FDA Food Code" -> viewModel.setRuleset("retailfood")
            "General Sanitation" -> viewModel.setRuleset("general")
            "Recreational Water" -> viewModel.setRuleset("recwater")
        }

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}

}