package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.home

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProgramList(val name: String, val safeArg: String, val icon: String)
