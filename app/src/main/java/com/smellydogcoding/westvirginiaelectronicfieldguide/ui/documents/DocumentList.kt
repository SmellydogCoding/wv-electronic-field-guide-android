package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DocumentList(val rules: List<RuleDocs>, val procedures: List<ProcedureDocs>, val reference: List<ReferenceDocs>)

@JsonClass(generateAdapter = true)
data class RuleDocs(val name: String, val path: String)
@JsonClass(generateAdapter = true)
data class ProcedureDocs(val name: String, val path: String)
@JsonClass(generateAdapter = true)
data class ReferenceDocs(val name: String, val path: String)

