package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.referenceTab

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.smellydogcoding.westvirginiaelectronicfieldguide.R
import com.smellydogcoding.westvirginiaelectronicfieldguide.databinding.TabProceduresFragmentBinding
import com.smellydogcoding.westvirginiaelectronicfieldguide.databinding.TabReferenceFragmentBinding
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.DocumentsSharedViewModel
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.proceduresTab.TabProceduresRecyclerViewAdapter

class TabReferenceFragment : Fragment() {

    private val sharedViewModel: DocumentsSharedViewModel by lazy { ViewModelProvider(requireActivity()).get(DocumentsSharedViewModel::class.java) }
    private lateinit var tabReferenceRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = TabReferenceFragmentBinding.inflate(layoutInflater)
        binding.viewModel = sharedViewModel

        val adapter = TabReferenceRecyclerViewAdapter(TabReferenceRecyclerViewAdapter.ReferencesListListener { name, filePath -> sharedViewModel.onDocumentClicked(name, filePath) })
        binding.tabReferenceRecyclerView.adapter = adapter

        tabReferenceRecyclerView = binding.tabReferenceRecyclerView
        tabReferenceRecyclerView.addItemDecoration(DividerItemDecoration(tabReferenceRecyclerView.context, DividerItemDecoration.VERTICAL))

        sharedViewModel.referenceDocumentsList.observe(viewLifecycleOwner, {
            it?.let {
                adapter.submitList(it)
            }
        })

        return binding.root
    }

}