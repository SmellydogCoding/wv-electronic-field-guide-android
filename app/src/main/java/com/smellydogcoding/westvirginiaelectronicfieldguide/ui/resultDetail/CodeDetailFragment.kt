package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.resultDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.smellydogcoding.westvirginiaelectronicfieldguide.R

class CodeDetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val description = CodeDetailFragmentArgs.fromBundle(requireArguments()).description
        val section = CodeDetailFragmentArgs.fromBundle(requireArguments()).section
        val fullText = CodeDetailFragmentArgs.fromBundle(requireArguments()).fullText
        val classification = CodeDetailFragmentArgs.fromBundle(requireArguments()).classification
        val category = CodeDetailFragmentArgs.fromBundle(requireArguments()).category
        val compliance = CodeDetailFragmentArgs.fromBundle(requireArguments()).compliance
        val followUpRecommended = CodeDetailFragmentArgs.fromBundle(requireArguments()).followUpRecommended
        val closure = CodeDetailFragmentArgs.fromBundle(requireArguments()).closure
        val notes = CodeDetailFragmentArgs.fromBundle(requireArguments()).notes

        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_code_detail, container, false)

        // TextViews
        val codeDescriptionTextview: TextView = root.findViewById(R.id.value_description)
        codeDescriptionTextview.text = description

        val codeClassificationLabelTextview: TextView = root.findViewById(R.id.label_classification)
        val codeClassificationTextview: TextView = root.findViewById(R.id.value_classification)
        showOrHide(classification, codeClassificationLabelTextview, codeClassificationTextview)

        val codeSectionTextview: TextView = root.findViewById(R.id.value_section)
        codeSectionTextview.text = section

        val codeCategoryLabelTextview: TextView = root.findViewById(R.id.label_category)
        val codeCategoryTextview: TextView = root.findViewById(R.id.value_category)
        showOrHide(category, codeCategoryLabelTextview, codeCategoryTextview)

        val codeComplianceLabelTextview: TextView = root.findViewById(R.id.label_compliance)
        val codeComplianceTextview: TextView = root.findViewById(R.id.value_compliance)
        showOrHide(compliance, codeComplianceLabelTextview, codeComplianceTextview)

        val codeFollowUpRecommendedLabelTextview: TextView = root.findViewById(R.id.label_followUpRecommended)
        val codeFollowUpRecommendedTextview: TextView = root.findViewById(R.id.value_followUpRecommended)
        showOrHide(followUpRecommended, codeFollowUpRecommendedLabelTextview, codeFollowUpRecommendedTextview)

        val codeClosureLabelTextview: TextView = root.findViewById(R.id.label_closure)
        val codeClosureTextview: TextView = root.findViewById(R.id.value_closure)
        showOrHide(closure, codeClosureLabelTextview, codeClosureTextview)

        val codeNotesLabelTextview: TextView = root.findViewById(R.id.label_notes)
        val codeNotesWebView: WebView = root.findViewById(R.id.value_notes)
        codeNotesWebView.settings.textZoom = 110
        codeNotesWebView.loadDataWithBaseURL(null, notes, "text/html", "base64", null)
        showOrHide(notes, codeNotesLabelTextview, codeNotesWebView)

        // Display fullText as a text view so that tables render correctly
        val fullTextWebView : WebView = root.findViewById(R.id.value_fullText)
        fullTextWebView.settings.textZoom = 110
        fullTextWebView.loadDataWithBaseURL(null, fullText, "text/html", "base64", null)

        return root
    }

    // hide TextViews that don't have a value
    private fun showOrHide(codeItem: String?, label: TextView, value: Any) {
        if (codeItem != null && codeItem != "") {
            label.visibility = View.VISIBLE
            when (value) {
                is TextView -> {
                    value.visibility = View.VISIBLE
                    value.text = codeItem
                    itemDecoration(codeItem, value)
                }
                is WebView -> value.visibility = View.VISIBLE
            }
        } else {
            label.visibility = View.GONE
            when (value) {
                is TextView -> value.visibility = View.GONE
                is WebView -> value.visibility = View.GONE
            }

        }
    }

    // change text color for certain values
    private fun itemDecoration(keyword: String, value: TextView) {
        when (keyword) {
            "Priority", "Yes" -> value.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.error
                )
            )
            "Priority Foundation", "Potential" -> value.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.warning
                )
            )
        }
    }
}
