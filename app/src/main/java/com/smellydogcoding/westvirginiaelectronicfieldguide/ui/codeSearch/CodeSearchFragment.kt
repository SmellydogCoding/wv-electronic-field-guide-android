package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.codeSearch

import android.app.Activity
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.smellydogcoding.westvirginiaelectronicfieldguide.R
import com.smellydogcoding.westvirginiaelectronicfieldguide.databinding.FragmentCodeSearchBinding


class CodeSearchFragment : Fragment() {

    private val viewModel: CodeSearchViewModel by lazy { ViewModelProvider(requireActivity()).get(
        CodeSearchViewModel::class.java
    ) }
    private lateinit var codeSearchRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        restoreSearchResults()

        val binding = FragmentCodeSearchBinding.inflate(layoutInflater)
        binding.viewModel = viewModel

        val spinner: Spinner = binding.codeSearchSpinner
        spinner.onItemSelectedListener = SpinnerActivity(viewModel)

        val searchInput : EditText = binding.codeSearchInput
        val searchButton : Button = binding.codeSearchButton

        val resultsCount : TextView = binding.resultsList

        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.code_search,
            R.layout.code_search_spinner_item
        )
            .also { adapter ->
                adapter.setDropDownViewResource(R.layout.code_search_spinner_item_dropdown)
                spinner.adapter = adapter
            }

        codeSearchRecyclerView = binding.codeSearchRecyclerView

        viewModel.results.observe(viewLifecycleOwner, {
            it?.let {
                when {
                    it.isEmpty() -> {
                        resultsCount.text = getString(
                            R.string.no_search_results,
                            viewModel.searchTerm.value
                        )
                        codeSearchRecyclerView.visibility = View.GONE
                    }
                    else -> {
                        resultsCount.text = getString(
                            R.string.search_results,
                            viewModel.results.value?.size,
                            viewModel.searchTerm.value
                        )
                        codeSearchRecyclerView.visibility = View.VISIBLE
                        val adapter = CodeSearchRecyclerViewAdapter(
                            it,
                            CodeSearchRecyclerViewAdapter.CodeSearchListListener { rule ->
                                viewModel.onDocumentClicked(
                                    rule
                                )
                            })
                        binding.codeSearchRecyclerView.adapter = adapter
                        codeSearchRecyclerView.addItemDecoration(
                            DividerItemDecoration(
                                codeSearchRecyclerView.context,
                                DividerItemDecoration.VERTICAL
                            )
                        )
                    }
                }
            }
        })

        viewModel.ruleset.observe(viewLifecycleOwner, {
            searchInput.text = null
            resultsCount.text = ""
        })

        viewModel.ruleIsClicked.observe(viewLifecycleOwner, {
            if (it) {
                val fullText = viewModel.fullText.value!!
                val code = viewModel.code.value!!
                val section = viewModel.section.value!!
                val description = viewModel.description.value!!
                val classification: String? = viewModel.classification.value
                val category: String? = viewModel.category.value
                val compliance: String? = viewModel.compliance.value
                val followUpRecommended: String? = viewModel.followUpRecommended.value
                val closure: String? = viewModel.closure.value
                val notes: String? = viewModel.notes.value
                this.findNavController().navigate(
                    CodeSearchFragmentDirections.actionCodeSearchFragmentToCodeDetailFragment(
                        fullText,
                        code,
                        section,
                        description,
                        classification,
                        category,
                        compliance,
                        followUpRecommended,
                        closure,
                        notes
                    )
                )
                viewModel.onResultsDetailsNavigated()
            }
        })

        searchButton.setOnClickListener {
            viewModel.search(searchInput.text.toString())
            hideKeyboardFrom(requireActivity().applicationContext, it)
        }

        searchInput.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                viewModel.search(searchInput.text.toString())
                hideKeyboardFrom(requireActivity().applicationContext, searchButton)
                return@OnKeyListener true
            }
            false
        })

        return binding.root

    }

    private fun hideKeyboardFrom(context: Context, view: View) {
        val imm: InputMethodManager = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onPause() {
        super.onPause()
        val sharedPreferences: SharedPreferences = requireContext().getSharedPreferences("SearchData", MODE_PRIVATE)
        val editor : SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString("SEARCH TERM", viewModel.searchTerm.value)
        editor.putString("RULE SET", viewModel.ruleset.value)
        editor.apply()
    }

    private fun restoreSearchResults() {
        val sharedPreferences: SharedPreferences = requireContext().getSharedPreferences("SearchData", MODE_PRIVATE)
        val searchTerm = sharedPreferences.getString("SEARCH TERM", "")
        val ruleSet = sharedPreferences.getString("RULE SET", "")
        if (searchTerm!!.isNotEmpty()) {
            viewModel.restoreSearch(searchTerm, ruleSet!!)
        }
    }
}