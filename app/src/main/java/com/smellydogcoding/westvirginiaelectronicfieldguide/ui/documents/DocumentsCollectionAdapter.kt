package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.proceduresTab.TabProceduresFragment
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.referenceTab.TabReferenceFragment
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.rulesTab.TabRulesFragment

class DocumentsCollectionAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment {
       return when (position) {
           0 -> TabRulesFragment()
           1 -> TabProceduresFragment()
           2 -> TabReferenceFragment()
           else -> TabRulesFragment()
       }
    }

}