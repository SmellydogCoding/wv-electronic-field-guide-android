package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.smellydogcoding.westvirginiaelectronicfieldguide.databinding.HomeGridViewItemBinding

class HomeAdapter(private val clickListener: ProgramListListener) : ListAdapter<ProgramList, HomeAdapter.ProgramListViewHolder>(
    HomeAdapterDiffCallback()
) {

    override fun onBindViewHolder(holder: ProgramListViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(clickListener, item)
    }

    class ProgramListViewHolder(private val binding: HomeGridViewItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(clickListener: ProgramListListener, item: ProgramList) {
            binding.program = item
            binding.programText.text = item.name
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProgramListViewHolder {
        return ProgramListViewHolder(HomeGridViewItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    class HomeAdapterDiffCallback : DiffUtil.ItemCallback<ProgramList>() {
        override fun areItemsTheSame(oldItem: ProgramList, newItem: ProgramList): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: ProgramList, newItem: ProgramList): Boolean {
            return oldItem == newItem
        }

    }

    class ProgramListListener(val clickListener: (safeArg: String, name: String) -> Unit) {
        fun onClick(program: ProgramList) = clickListener(program.safeArg, program.name)
    }

}
