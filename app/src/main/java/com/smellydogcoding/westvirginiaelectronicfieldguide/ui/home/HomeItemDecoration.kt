package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.home

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class HomeItemDecoration(spacing: Int): RecyclerView.ItemDecoration() {

    private val margin = spacing

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.top = margin
        outRect.right = margin
        outRect.bottom = margin
        outRect.left = margin
    }
}