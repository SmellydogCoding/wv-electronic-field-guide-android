package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.proceduresTab

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.smellydogcoding.westvirginiaelectronicfieldguide.R
import com.smellydogcoding.westvirginiaelectronicfieldguide.databinding.TabProceduresFragmentBinding
import com.smellydogcoding.westvirginiaelectronicfieldguide.databinding.TabRulesFragmentBinding
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.DocumentsSharedViewModel
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.rulesTab.TabRulesRecyclerViewAdapter

class TabProceduresFragment : Fragment() {

    private val sharedViewModel: DocumentsSharedViewModel by lazy { ViewModelProvider(requireActivity()).get(DocumentsSharedViewModel::class.java) }
    private lateinit var tabProceduresRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = TabProceduresFragmentBinding.inflate(layoutInflater)
        binding.viewModel = sharedViewModel

        val adapter = TabProceduresRecyclerViewAdapter(TabProceduresRecyclerViewAdapter.ProceduresListListener { name, filePath -> sharedViewModel.onDocumentClicked(name, filePath) })
        binding.tabProceduresRecyclerView.adapter = adapter

        tabProceduresRecyclerView = binding.tabProceduresRecyclerView
        tabProceduresRecyclerView.addItemDecoration(DividerItemDecoration(tabProceduresRecyclerView.context, DividerItemDecoration.VERTICAL))

        sharedViewModel.proceduresDocumentsList.observe(viewLifecycleOwner, {
            it?.let {
                adapter.submitList(it)
            }
        })

        return binding.root
    }

}