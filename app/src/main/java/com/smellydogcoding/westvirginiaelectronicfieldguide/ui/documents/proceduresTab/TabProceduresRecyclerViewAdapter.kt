package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.proceduresTab

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.smellydogcoding.westvirginiaelectronicfieldguide.databinding.ProceduresListViewItemBinding
import com.smellydogcoding.westvirginiaelectronicfieldguide.databinding.RulesListViewItemBinding
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.ProcedureDocs

class TabProceduresRecyclerViewAdapter(private val clickListener: ProceduresListListener) : ListAdapter<ProcedureDocs, TabProceduresRecyclerViewAdapter.ProceduresListViewHolder>(TabProceduresAdapterDiffCallback()) {

    class ProceduresListViewHolder(private val binding: ProceduresListViewItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(clickListener: ProceduresListListener, item: ProcedureDocs) {
            binding.procedure = item
            binding.procedureText.text = item.name
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }
    }

    class TabProceduresAdapterDiffCallback : DiffUtil.ItemCallback<ProcedureDocs>() {
        override fun areItemsTheSame(oldItem: ProcedureDocs, newItem: ProcedureDocs): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: ProcedureDocs, newItem: ProcedureDocs): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProceduresListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = ProceduresListViewItemBinding.inflate(inflater)
        itemView.procedureHolder.minWidth = parent.width
        return ProceduresListViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ProceduresListViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(clickListener, item)
    }

    class ProceduresListListener(val clickListener: (name: String, filePath: String) -> Unit) {
        fun onClick(rule: ProcedureDocs) = clickListener(rule.name, rule.path)
    }

}