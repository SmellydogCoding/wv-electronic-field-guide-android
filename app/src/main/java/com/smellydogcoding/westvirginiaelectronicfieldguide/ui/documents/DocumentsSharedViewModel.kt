package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.JsonAsset

class DocumentsSharedViewModel : ViewModel() {

    private val _rulesDocumentsList = MutableLiveData<List<RuleDocs>>()
    val rulesDocumentsList : LiveData<List<RuleDocs>> get() = _rulesDocumentsList

    private val _proceduresDocumentsList = MutableLiveData<List<ProcedureDocs>>()
    val proceduresDocumentsList : LiveData<List<ProcedureDocs>> get() = _proceduresDocumentsList

    private val _referenceDocumentsList = MutableLiveData<List<ReferenceDocs>>()
    val referenceDocumentsList : LiveData<List<ReferenceDocs>> get() = _referenceDocumentsList

    private val _navigateToPDFViewerName = MutableLiveData<String>()
    val navigateToPDFViewerName: LiveData<String> get() = _navigateToPDFViewerName

    private val _navigateToPDFViewerFilePath = MutableLiveData<String>()
    val navigateToPDFViewerFilePath : LiveData<String> get() = _navigateToPDFViewerFilePath

    fun getDocs(program: String) {
        val data: List<DocumentList>? = JsonAsset.getJson("${program}/${program}.json")
        _rulesDocumentsList.value = (data!!.map { it.rules })[0]
        _proceduresDocumentsList.value = (data.map { it.procedures })[0]
        _referenceDocumentsList.value = (data.map { it.reference })[0]
    }

    fun onDocumentClicked(name: String, filePath: String) {
        _navigateToPDFViewerName.value = name
        _navigateToPDFViewerFilePath.value = filePath
    }

    fun onPDFViewerNavigated() {
        _navigateToPDFViewerName.value = null
        _navigateToPDFViewerFilePath.value = null
    }

}