package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.pdfviewer

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle
import com.github.barteksc.pdfviewer.util.FitPolicy
import com.smellydogcoding.westvirginiaelectronicfieldguide.R
import kotlinx.android.synthetic.main.fragment_pdfviewer.*

class PDFViewerFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val filePath = PDFViewerFragmentArgs.fromBundle(requireArguments()).filePath

        val root = inflater.inflate(R.layout.fragment_pdfviewer, container, false)

        val pdfView : PDFView = root.findViewById(R.id.pdfView)

        pdfView.fromAsset(filePath)
            .scrollHandle(DefaultScrollHandle(this.context))
            .load()

        return root
    }
}