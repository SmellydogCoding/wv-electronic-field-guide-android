package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.home

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.smellydogcoding.westvirginiaelectronicfieldguide.databinding.FragmentHomeBinding
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.JsonAsset


class HomeFragment : Fragment() {

    private val homeViewModel: HomeViewModel by lazy { ViewModelProvider(this).get(HomeViewModel::class.java) }
    private lateinit var homeRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        clearSavedSearch()

        // Utils.kt
        JsonAsset.application = context?.applicationContext as Application

        val binding = FragmentHomeBinding.inflate(layoutInflater)
        binding.viewModel = homeViewModel

        val adapter = HomeAdapter(HomeAdapter.ProgramListListener { safeArg, name -> homeViewModel.onProgramClicked(safeArg, name) })
        binding.homeRecyclerView.adapter = adapter

        homeRecyclerView = binding.homeRecyclerView
        homeRecyclerView.addItemDecoration(HomeItemDecoration(24))

        homeViewModel.programList.observe(viewLifecycleOwner, {
            it?.let {
                adapter.submitList(it)
            }
        })

        homeViewModel.navigateToDocumentsProgramId.observe(viewLifecycleOwner, {
            it?.let {
                if (it == "search") {
                    this.findNavController().navigate(HomeFragmentDirections.actionNavHomeToCodeSearchFragment2())
                } else {
                    val name = homeViewModel.navigateToDocumentsProgramName.value.toString()
                    this.findNavController().navigate(HomeFragmentDirections.actionNavHomeToDocumentsFragment(it, name))
                }
                homeViewModel.onDocumentsNavigated()
            }
        })

        return binding.root
    }

    private fun clearSavedSearch() {
        val sharedPreferences: SharedPreferences = requireContext().getSharedPreferences("SearchData", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }
}