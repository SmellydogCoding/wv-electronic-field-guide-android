package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.codeSearch

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RetailFoodRuleSet(val FullText: String, val Code: String, val Classification: String, val Description: String, val Category: String, val Section: String, val Compliance: String)

@JsonClass(generateAdapter = true)
data class GeneralSanitationRuleSet(val Code: String, val Section: String, val Description: String, val FollowUpRecommended: String, val FullText: String)

@JsonClass(generateAdapter = true)
data class RecreationalWaterRuleSet(val Code: String, val Section: String, val Description: String, val Closure: String, val FullText: String, val Notes: String)

