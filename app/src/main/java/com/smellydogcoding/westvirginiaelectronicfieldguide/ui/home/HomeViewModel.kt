package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.JsonAsset

class HomeViewModel : ViewModel() {

    private val _programList = MutableLiveData<List<ProgramList>>()
    val programList: LiveData<List<ProgramList>> get() = _programList

    private val _navigateToDocumentsProgramId = MutableLiveData<String>()
    val navigateToDocumentsProgramId get() = _navigateToDocumentsProgramId

    private val _navigateToDocumentsProgramName = MutableLiveData<String>()
    val navigateToDocumentsProgramName get() = _navigateToDocumentsProgramName

    init {
        // Utils.kt
        _programList.value = JsonAsset.getJson("programs/home.json")
    }

    fun onProgramClicked(safeArg: String, name: String) {
        _navigateToDocumentsProgramName.value = name
        _navigateToDocumentsProgramId.value = safeArg
    }

    fun onDocumentsNavigated() {
        _navigateToDocumentsProgramId.value = null
        _navigateToDocumentsProgramName.value = null
    }

}