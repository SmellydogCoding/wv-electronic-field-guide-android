package com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.rulesTab

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.smellydogcoding.westvirginiaelectronicfieldguide.databinding.TabRulesFragmentBinding
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.documents.DocumentsSharedViewModel

class TabRulesFragment : Fragment() {

    private val sharedViewModel: DocumentsSharedViewModel by lazy { ViewModelProvider(requireActivity()).get(DocumentsSharedViewModel::class.java) }
    private lateinit var tabRulesRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = TabRulesFragmentBinding.inflate(layoutInflater)
        binding.viewModel = sharedViewModel

        val adapter = TabRulesRecyclerViewAdapter(TabRulesRecyclerViewAdapter.RulesListListener { name, filePath -> sharedViewModel.onDocumentClicked(name, filePath) })
        binding.tabRulesRecyclerView.adapter = adapter

        tabRulesRecyclerView = binding.tabRulesRecyclerView
        tabRulesRecyclerView.addItemDecoration(DividerItemDecoration(tabRulesRecyclerView.context, DividerItemDecoration.VERTICAL))

        sharedViewModel.rulesDocumentsList.observe(viewLifecycleOwner, {
            it?.let {
                adapter.submitList(it)
            }
        })

        return binding.root
    }

}