package com.smellydogcoding.westvirginiaelectronicfieldguide

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.android.material.navigation.NavigationView
import com.smellydogcoding.westvirginiaelectronicfieldguide.ui.home.HomeFragmentDirections

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)

        /**
         * Setup Navigation for this Activity
         * When using FragmentContainerView you need to find the NavController using
         * findFragmentById() instead of findNavController().  This is necessary when
         * initializing the nav controller from the onCreate() method.
         */

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController

        // using the listener instead of passing an array of menu id's
        navView.setNavigationItemSelectedListener(NavigationView.OnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_search -> {
                    navController.navigate(HomeFragmentDirections.actionNavHomeToCodeSearchFragment2())
                }
                R.id.nav_food -> {
                    navController.navigate(HomeFragmentDirections.actionNavHomeToDocumentsFragment("food", "Food Establishments"))
                }
                R.id.nav_general -> {
                    navController.navigate(HomeFragmentDirections.actionNavHomeToDocumentsFragment("general", "General Sanitation"))
                }
                R.id.nav_water -> {
                    navController.navigate(HomeFragmentDirections.actionNavHomeToDocumentsFragment("water", "Water Wells"))
                }
                R.id.nav_sewage -> {
                    navController.navigate(HomeFragmentDirections.actionNavHomeToDocumentsFragment("sewage", "Sewage"))
                }
                R.id.nav_recwater -> {
                    navController.navigate(HomeFragmentDirections.actionNavHomeToDocumentsFragment("recwater", "Recreational Water"))
                }
                R.id.nav_bodyart -> {
                    navController.navigate(HomeFragmentDirections.actionNavHomeToDocumentsFragment("bodyart", "Body Art"))
                }
                R.id.nav_mobilehome -> {
                    navController.navigate(HomeFragmentDirections.actionNavHomeToDocumentsFragment("mobilehome", "Mobile Home Parks"))
                }
                R.id.nav_childcare -> {
                    navController.navigate(HomeFragmentDirections.actionNavHomeToDocumentsFragment("childcare", "Child Care"))
                }
                R.id.nav_rabies -> {
                    navController.navigate(HomeFragmentDirections.actionNavHomeToDocumentsFragment("rabies", "Rabies and Vector Control"))
                }
                R.id.nav_disaster -> {
                    navController.navigate(HomeFragmentDirections.actionNavHomeToDocumentsFragment("disaster", "Disaster Sanitation"))
                }
                R.id.nav_about -> {
                    navController.navigate(HomeFragmentDirections.actionNavHomeToAboutFragment())
                }
            }
            drawerLayout.closeDrawers()
            return@OnNavigationItemSelectedListener true
        })

        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(navController.graph, drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

}